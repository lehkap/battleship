﻿using Common.DTO;
using Entity;
using Entity.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Respose
{
    public class CurentGameResponse :BaseResponse
    {
        /// <summary>
        /// Информация о сопернике
        /// </summary>
        [DataMember]
        public DTOUser Opponent { get; set; }
    }
}

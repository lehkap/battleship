﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO
{
    [DataContract]
    public class DTOFullUserInfo
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public DateTime DataRegister { get; set; }

        [DataMember]
        public int Games { get; set; }

        [DataMember]
        public int PercentWonGames { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Enums;
using System.Runtime.Serialization;

namespace Entity.GameEntities
{
    [DataContract]
    public class Deck
    {
        [DataMember]
        public DeckStatus Status { get; private set; } = DeckStatus.Ok;

        public void Fire()
        {
            Status = DeckStatus.Damaged;
        }
    }
}

﻿using System.Runtime.Serialization;

namespace Entity.Enums
{
    /// <summary>
    /// Ориентация корабля.
    /// </summary>
    [DataContract]
    public enum ShipOrientation
    {
        [EnumMember]
        Horisontal,
        [EnumMember]
        Vertical
    }
}

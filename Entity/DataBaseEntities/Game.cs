﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.DataBaseEntities
{
    [Table("Game")]
    public class Game
    {
        [Key]
        public Guid Id { get; set; }

        //[ForeignKey("Player1")]
        public Guid IdPlayer1 { get; set; }

        //[ForeignKey("Player2")]
        public Guid IdPlayer2 { get; set; }

        public DateTime CreateDateTime { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        //[ForeignKey("Winner")]
        public Guid IdWinner { get; set; }

        [NotMapped]
        public bool IsAvailable
        {
            get
            {
                if (IdPlayer2 == Guid.Empty)
                    return true;
                return false;
            }
        }
        public User Player1 { get; set; }

        public User Player2 { get; set; }

        public User Winner { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Common.DTO;

namespace Common.Respose
{
    public class GetTopPlayersResponse : BaseResponse
    {
        [DataMember]
        public List<DTOFullUserInfo> TopPlayers { get; set; }
    }
}

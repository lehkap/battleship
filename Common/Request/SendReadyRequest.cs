﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.GameEntities;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Common.DTO;

namespace Common.Request
{
    public class SendReadyRequest:BaseRequest
    {
        [DataMember]
        public DTOShip[] Ships { get; set; } 
    }
}

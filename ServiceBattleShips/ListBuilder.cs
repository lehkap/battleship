﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.DTO;

namespace ServiceBattleShips
{
    public class ListBuilder
    {
       public List<DTOAwaitingGame> GetGameList(List<DuoGame> gameList)
        {
            var result = new List<DTOAwaitingGame>();
            foreach (var item in gameList)
            {
                var game = new DTOAwaitingGame()
                {
                    Creator = item.Players.First(),
                    CreationTime = item.CreateDateTime,
                    GameId = item.GameId
                };
                result.Add(game);
            }
            return result;
        }
    }
}
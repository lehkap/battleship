﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebClientBattleShip.Models;

namespace WebClientBattleShip.Controllers
{
    public static class ControllerExtensions
    {
        /// <summary>
        /// Вытаскиваем GUID из куков, если пользователь с такими куками залогинен - возвращаем куки. В противном случае заводим ему местную уч.запись
        /// </summary>
         public static Guid WorkWithCookie(this Controller controller)
        {
           var guid = controller.GetGuidFromCookie();
            if (!SingletonModel.GetInstance().Clients.ContainsKey(guid))
            {
                guid = Guid.NewGuid();
                controller.AddGuidToCookie(guid);
                var client =
                    new Client(new ClientCallback());
                SingletonModel.GetInstance().Clients.Add(guid, client);
            }
            return guid;
        }
        /// <summary>
        /// Добавить в куки Guid
        /// </summary>
        public static void AddGuidToCookie(this Controller controller, Guid guid)
        {
            var clientCookie = new HttpCookie("UserInfo") {["SessionKey"] = guid.ToString()};
            controller.Response.Cookies.Add(clientCookie);
        }
        /// <summary>
        /// Получить из куков Guid
        /// </summary>

        public static Guid GetGuidFromCookie(this Controller controller)
        {
            Guid guid;
            var cookie = controller.Request.Cookies["UserInfo"]?["SessionKey"];
            Guid.TryParse(cookie, out guid);
            return guid;
        }
    }
}
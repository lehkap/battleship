﻿using Common.DTO;
using Entity;
using Entity.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Respose
{
    public class GetListGamesResponse : BaseResponse
    {
        [DataMember]
        public List<DTOAwaitingGame> Games {get; set; }
    }
}

﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Host
{
    class Program
    {
        private static Logger _log = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            try
            {
                ServiceHost serviceHost;
                serviceHost = new ServiceHost(typeof(ServiceBattleShips.Service));
                using (serviceHost)
                {

                    serviceHost.Open();
                    Console.WriteLine("Host Started");

                    _log.Info("Host Started");


                    Console.ReadLine();
                }
            }
            catch(Exception ex)
            {
                _log.Fatal("Host NOT STARTED Exception: {0}", ex.Message);
            }
        }
    }
}

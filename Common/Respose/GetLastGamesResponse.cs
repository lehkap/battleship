﻿using Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Respose
{
    public class GetLastGamesResponse : BaseResponse
    {
        [DataMember]
        public List<DTOFullGameInfo> Games { get; set; }
    }
}

﻿using System.Runtime.Serialization;

namespace Entity.GameEntities
{
    /// <summary>
    /// Координата.
    /// </summary>
    [DataContract]
    public class XYCoordinate
    {
        /// <summary>
        /// Координата по горизонтали.
        /// </summary>
        [DataMember]
        public int X { get; set; }
        /// <summary>
        /// Координата по вертикали.
        /// </summary>
        [DataMember]
        public int Y { get; set; }
    }
}

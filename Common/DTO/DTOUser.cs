﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.GameEntities;
using System.Runtime.Serialization;

namespace Common.DTO
{
    [DataContract]
    public class DTOUser
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Login { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public Guid CurrentGameId { get; set; }
        [DataMember]
        public int Games { get; set; }
        [DataMember]
        public int GamesWon { get; set; }
    }
}

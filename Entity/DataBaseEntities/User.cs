﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity.DataBaseEntities
{
    [Table("User")]
    public class User
    {   [Column(Order =1)]
        [Key]
        public Guid Id { get; set; }
        [Column(Order = 2)]
        [Key]
        public string Login { get; set; }

        public string Password { get; set; }
        [Column(Order = 3)]
        [Key]
        public string Email { get; set; }

        public DateTime DataRegister { get; set; }
        public int Games { get; set; }
        public int WonGames { get; set; }
    }
}

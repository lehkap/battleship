﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.DTO;
using Entity.Enums;
using Entity.GameEntities;

namespace ServiceBattleShips
{
    public class DuoGame
    {
        public Guid GameId { get;}
        public DateTime CreateDateTime { get; }
        public List<DTOUser> Players { get; }
        public Dictionary<DTOUser, BattleMap> BattleMaps { get; }
        public DateTime StartGameDateTime { get; private set; }
        public DTOUser NextUser { get; private set; }
        public DTOUser Winner { get; internal set; }
        public bool IsGameOver { get; internal set; }
        public int DurationGameInSeconds { get; private set; }
        /// <summary>
        /// Конструктор игры.
        /// </summary>
        /// <param name="user">Создатель игры.</param>
        /// <param name="map">Карта создателя игры.</param>
        public DuoGame(DTOUser user)///, BattleMap map)
        {
            BattleMaps = new Dictionary<DTOUser, BattleMap>();
            GameId = Guid.NewGuid();
            CreateDateTime = DateTime.Now;
            Players = new List<DTOUser>() { user };
            IsGameOver = new bool();
        }
        /// <summary>
        /// Добавить оппонента
        /// </summary>
        /// <param name="user">Юзер.</param>
        /// <param name="map">Карта юзера.</param>
        /// <returns>Возращает значение, удалось ли присоединиться к игре.</returns>
        public bool AddOpponent(DTOUser user)//, BattleMap map)
        {
            if (Players.Count == 2)
                return false;
            //BattleMaps.Add(user,map);
            Players.Add(user);
            var randomPlayerId = new Random();
            NextUser = Players.First();//Players.ElementAt(randomPlayerId.Next(0, 1));
            StartGameDateTime = DateTime.Now;
            return true;
        }
        /// <summary>
        /// Сделать выстрел.
        /// </summary>
        /// <param name="user">Атакуемый.</param>
        /// <param name="shot">Выстрел.</param>
        /// <returns>Возвращает значение, которое означает закончилась ли игра.</returns>
        public Shot Fire(DTOUser user, Shot shot)
        {
            shot.Status = BattleMaps[user].Fire(shot);
            if (shot.Status == ShotStatus.Hit || shot.Status == ShotStatus.Killed) // если попал или убил продолжаешь ход.
            {
                NextUser = Players.First(p=>p!=user);
            }
            else
            {
                NextUser = user;
            }
            var isGameOver = !BattleMaps[user].IsShipsAlive;
            if (isGameOver)
            {
                Winner = Players.First(p => p != user);
                DurationGameInSeconds = (DateTime.Now - StartGameDateTime).Seconds;
            }
            IsGameOver= isGameOver;
            return shot;
        }

        /// <summary>
        /// Добавляет карту пользователя когда он готов
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <param name="battleMap">Карта</param>
        public void AddBattleMap(DTOUser user, BattleMap battleMap)
        {
            BattleMaps.Add(user, battleMap);
        }

        /// <summary>
        /// Проверяет готовность обоих игроков к игре
        /// </summary>
        /// <returns>True - готовы, False- нет</returns>
        public bool AllClientsIsReady()
        {
            if (BattleMaps.Count==2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
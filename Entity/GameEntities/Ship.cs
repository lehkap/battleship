﻿
using System.Linq;
using Entity.Enums;
using System.Runtime.Serialization;

namespace Entity.GameEntities
{
    [DataContract]
    public class Ship
    {
        [DataMember]
        public XYCoordinate StartPoint { get; private set; }
        [DataMember]
        public ShipOrientation Orientation { get; private set; }
        [DataMember]
        public Deck[] Decks { get; }
        [DataMember]
        public bool IsKilled { get; set; }
        

        public Ship(int numDeck, XYCoordinate startPoint, ShipOrientation orientation)
        {
            Decks = new Deck[numDeck];
            for (var i = 0; i < numDeck; i++)
            {
                Decks[i] = new Deck();
            }
            StartPoint = startPoint;
            Orientation = orientation;
            IsKilled = false;
        }

        public void Fire(int numDeck)
        {
            Decks[numDeck].Fire();
            if (Decks.All(d=>d.Status==DeckStatus.Damaged))
            {
                IsKilled = true;
            }
        }
    }
}

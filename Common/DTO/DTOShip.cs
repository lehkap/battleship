﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Entity.Enums;
using Entity.GameEntities;

namespace Common.DTO
{
    [DataContract]
    public class DTOShip
    {
        [DataMember]
        public int DeckCount { get; set; }
        [DataMember]
        public ShipOrientation Orientation { get; set; }
        [DataMember]
        public XYCoordinate Coordinates { get; set; }
    }
}

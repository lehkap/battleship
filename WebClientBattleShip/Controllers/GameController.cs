﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebClientBattleShip.GameService;
using WebClientBattleShip.Models;
using WebClientBattleShip.Models.DtoModels;

namespace WebClientBattleShip.Controllers
{
    public class GameController : Controller
    {
        #region Main Views
        public ActionResult Index()
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            if (client.IsAuthorised)
            {
                client.ServiceClient.GetListAvailableGames();
                ViewBag.IsLogined = true;
                return View();
            }
            return new RedirectResult("~/Home/Index");
        }

        [HttpGet]
        public ActionResult GamesAvailable()
        {

            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            ViewBag.UserId = client.ClientId;
            if (client.IsAuthorised)
            {
                var availableGamesResponses = client.Callbacks.GetResponses(typeof (GetListGamesResponse));
                if (availableGamesResponses.Count > 0)
                {
                    var availableGamesResponse = availableGamesResponses.Last() as GetListGamesResponse;
                    availableGamesResponses.Remove(availableGamesResponse);
                    client.Callbacks.ClearResponses(availableGamesResponses);
                    if (availableGamesResponse.IsSuccess)
                    {
                        return View(availableGamesResponse.Games);
                    }
                    ViewBag.Error = availableGamesResponse.Error;
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult Statistics(string id = "")
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            if (client.IsAuthorised)
            {
                ViewBag.IsLogined = true;
                return View();
            }
            ViewBag.IsLogined = false;
            return new RedirectResult("~/Home/Index");
        }

        public ActionResult Game()
        {
            return View();
        }
        #endregion
        #region Partial Views
                public ActionResult TopPlayers()
                {
                    var userid = this.WorkWithCookie();
                    var client = SingletonModel.GetInstance().Clients[userid];
                    ViewBag.NameUser = client.Name;
                    if (client.IsAuthorised)
                    {
                        client.ServiceClient.GetTopPlayers();
                        var topPlayersResponses = client.Callbacks.GetResponses(typeof (GetTopPlayersResponse));
                        if (topPlayersResponses.Count > 0)
                        {
                            client.Callbacks.ClearResponses(topPlayersResponses);
                            var topPlayersResponse = topPlayersResponses.Last() as GetTopPlayersResponse;
                            if (topPlayersResponse.IsSuccess)
                            {
                                return View(topPlayersResponse.TopPlayers);
                            }
                            ViewBag.Error = topPlayersResponse.Error;
                        }
                    }
                    return View();
                }

                public ActionResult LastGames()
                {
                    var userid = this.WorkWithCookie();
                    var client = SingletonModel.GetInstance().Clients[userid];
                    ViewBag.NameUser = client.Name;
                    if (client.IsAuthorised)
                    {
                        client.ServiceClient.GetLastGames();
                        var lastGamesResponses = client.Callbacks.GetResponses(typeof (GetLastGamesResponse));
                        if (lastGamesResponses.Count > 0)
                        {
                            client.Callbacks.ClearResponses(lastGamesResponses);
                            var lastGameResponse = lastGamesResponses.Last() as GetLastGamesResponse;
                            if (lastGameResponse.IsSuccess)
                            {
                                return View(lastGameResponse.Games);
                            }
                            ViewBag.Error = lastGameResponse.Error;
                        }
                    }

                    return View();
                }


      
                public ActionResult BattleMap(bool enemy = false)
                {
                    return enemy?View(): View(TempData["Ships"]);
                }
        #endregion
        #region Actions
        [HttpGet]
        public ActionResult Play(string gameId)
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            if (client.IsAuthorised)
            {
                Guid gameGuid;
                if (Guid.TryParse(gameId, out gameGuid))
                {
                    var request = new ConnectToGameRequest() {GameId = gameGuid };
                    client.ServiceClient.ConnectToGame(request);
                    return new EmptyResult();
                }
                return new HttpStatusCodeResult(400);
            }
            return new RedirectResult("~/Home/Index");
        }

        [HttpGet]
        public ActionResult CreateGame()
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            if (client.IsAuthorised)
            {
                var request = new CreateGameRequest();
                client.ServiceClient.CreateGame(request);
                return new EmptyResult();
            }
            return new RedirectResult("~/Home/Index");
        }

        public bool AbortGame()
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            if (client.IsAuthorised)
            {
                var abortResponses = client.Callbacks.GetResponses(typeof (AbortGameResponse));
                if (abortResponses.Count > 0)
                {
                    client.Callbacks.ClearResponses(abortResponses);
                    return true;
                }
            }
            return false;
        }
        public ActionResult CheckOpponent()
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            if (!client.IsAuthorised)
            {
                return new RedirectResult("~/Home/Index");
            }
            var currentGameResponses = client.Callbacks.GetResponses(typeof(CurentGameResponse));
            if (currentGameResponses.Count > 0)
            {
                var currentGameResponse = currentGameResponses.Last() as CurentGameResponse;
                client.Callbacks.ClearResponses(currentGameResponses);
                if (currentGameResponse.IsSuccess)
                {
                    return View("ShipPlacement");
                }
            }
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult LeaveGame()
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            if (client.IsAuthorised)
            {
                client.ServiceClient.LeaveGame();
                return new EmptyResult();
            }
            return new RedirectResult("~/Home/Index");
        }

        [HttpPost]
        public string StartGame(Ships[] id)
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            if (id != null)
            {
                var shipsmassive = new DTOShip[10];
                for (var i = 0; i < id.Length; i++)
                {
                    shipsmassive[i] = new DTOShip()
                    {
                        Coordinates = new XYCoordinate() { X = id[i].x, Y = id[i].y },
                        DeckCount = id[i].length,
                        Orientation = id[i].position == "h" ? ShipOrientation.Horisontal : ShipOrientation.Vertical
                    };
                }
                TempData["Ships"] = id;
                var request = new SendReadyRequest() { Ships = shipsmassive };
                client.ServiceClient.SendReady(request);
                return "ships is sended";
            }
            var sendReadyResponses = client.Callbacks.GetResponses(typeof(SendReadyResponse));
            if (sendReadyResponses.Count > 0)
            {
                client.Callbacks.ClearResponses(sendReadyResponses);
                var sendReadyResponse = (sendReadyResponses.Last() as SendReadyResponse);
                if (sendReadyResponse.IsSuccess)
                {
                    return "ships is accepted";
                }
                return "ships are not accepted";
            }
            var opponentReadyResponses = client.Callbacks.GetResponses(typeof(SendOpponentIsReadyResponse));

            if (opponentReadyResponses.Count > 0)
            {
                client.Callbacks.ClearResponses(opponentReadyResponses);
                var opponentReadyResponse = (opponentReadyResponses.Last() as SendOpponentIsReadyResponse);
                if (opponentReadyResponse.IsSuccess)
                {
                    return "opponent is ready";
                }
                ViewBag.Error = opponentReadyResponse.Error;
            }
            var gameStaredResponses = client.Callbacks.GetResponses(typeof(StartGameResponse));
            if (gameStaredResponses.Count > 0)
            {
                var gameStaredResponse = (gameStaredResponses.Last() as StartGameResponse);
                if (gameStaredResponse.IsSuccess)
                {
                    return "game is started, " + (gameStaredResponse.NextShotUserId == client.ClientId ? "your step" : "enemy step");
                }
                ViewBag.Error = gameStaredResponse.Error;
            }
            return "";
        }

        public ActionResult UpdateChat(string message)
        {
            var userId = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userId];
            ViewBag.NameUser = client.Name;
            if (client.IsAuthorised)
            {
                var messageChatResponses = client.Callbacks.GetResponses(typeof(RecieveMessageResponse));
                if (messageChatResponses.Count > 0)
                {
                    client.Callbacks.ClearResponses(messageChatResponses);
                    foreach (var messageChat in messageChatResponses)
                    {
                        var messageChatResponse = messageChat as RecieveMessageResponse;
                        var chatMessage = new ChatMessage()
                        {
                            Message = messageChatResponse.Message,
                            Date = messageChatResponse.DateTime,
                            UserName = messageChatResponse.Name
                        };
                        client.ChatMessage.Add(chatMessage);
                    }
                }
                if (message != null)
                {
                    var request = new SendMessageRequest() { Message = message };
                    client.ServiceClient.SendMessage(request);
                    client.ChatMessage.Add(new ChatMessage()
                    {
                        Date = DateTime.Now,
                        Message = message,
                        UserName = client.Name
                    });
                }

                return View("Chat", client.ChatMessage);
            }
            return new RedirectResult("~/Home/Index/");
        }

        public JsonResult Shot(int x = -1, int y = -1)
        {
            var userId = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userId];

            if (x != -1 && y != -1)
            {
                client.ServiceClient.DoShot(new Shot() { XyCoordinate = new XYCoordinate() { X = x, Y = y } });
            }
            var shotResponses = client.Callbacks.GetResponses(typeof(ShotResponse));
            if (shotResponses.Count > 0)
            {
                var shotResponse = (shotResponses.Last() as ShotResponse);
                client.Callbacks.ClearResponses(shotResponses);
                if (shotResponse.IsSuccess)
                {
                    var result = new ShotResult()
                    {
                        IsKilled = shotResponse.CurrentShot.Status == ShotStatus.Killed,
                        X = shotResponse.CurrentShot.XyCoordinate.X,
                        Y = shotResponse.CurrentShot.XyCoordinate.Y,
                        IsMissed = shotResponse.CurrentShot.Status == ShotStatus.Missed,
                        IsYourStep = shotResponse.User.Id == client.ClientId,
                        IsYourNext = shotResponse.NextShotUserId == client.ClientId,
                    };
                    if (shotResponse.KilledShipInfo != null)
                    {
                        result.Ship =
                            new Ships()
                            {
                                length = shotResponse.KilledShipInfo.DeckCount,
                                x = shotResponse.KilledShipInfo.Coordinates.X,
                                y = shotResponse.KilledShipInfo.Coordinates.Y,
                                position =
                                    shotResponse.KilledShipInfo.Orientation == ShipOrientation.Horisontal ? "h" : "v"
                            };
                    }
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            var endGameResponses = client.Callbacks.GetResponses(typeof(EndGameResponse));
            if (endGameResponses.Count > 0)
            {
                client.Callbacks.ClearResponses(endGameResponses);
                var endGameResponse = endGameResponses.First() as EndGameResponse;
                if (endGameResponse.IsSuccess)
                {
                    return Json(new { WinnerName = endGameResponse.Winner.Login, Winner = endGameResponse.Winner.Id == client.ClientId ? "you" : "enemy" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
#endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Request
{
    public class SendMessageRequest:BaseRequest
    {
        [DataMember]
        public string Message { get; set; }
    }
}

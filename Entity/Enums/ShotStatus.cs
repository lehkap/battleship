﻿using System.Runtime.Serialization;

namespace Entity.Enums
{
    /// <summary>
    /// Результат выстрела по кораблю.
    /// </summary>
    [DataContract]
    public enum ShotStatus
    {
        [EnumMember]
        Missed,
        [EnumMember]
        Hit,
        [EnumMember]
        Killed
    }
}

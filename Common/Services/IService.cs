﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Common.Request;
using Common.Respose;
using Entity;
using Entity.GameEntities;

namespace Common.Services
{
    [ServiceContract(CallbackContract = typeof(IServiceCallback))]
    public interface IService
    {
        /// <summary>
        /// Авторизация
        /// </summary>
        /// <param name="request">Запрос на авторизацию</param>
        /// <returns>Ответ о авторизации</returns>
        [OperationContract(IsOneWay = false)]
        AuthorizeResponse Authorize(AuthorizeRequest request);

        /// <summary>
        /// Регистрация
        /// </summary>
        /// <param name="request">Запрос на регистрацию</param>
        /// <returns>Ответ о регистрации</returns>
        [OperationContract(IsOneWay = false)]
        RegisterResponse Register(RegisterRequest request);

        [OperationContract(IsOneWay = true)]
        void CreateGame(CreateGameRequest request);
        /// <summary>
        /// Покинуть игру
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void LeaveGame();
        /// <summary>
        /// Сделать выстрел.
        /// </summary>
        /// <param name="shot">Выстрел</param>
        [OperationContract(IsOneWay = true)]
        void DoShot(Shot shot);

        /// <summary>
        /// Присоединиться к игре
        /// </summary>
        /// <param name="request">Запрс с информацией для присоединения к игре</param>
        /// <returns></returns>
        [OperationContract(IsOneWay = true)]
        void ConnectToGame(ConnectToGameRequest request);

        /// <summary>
        /// Завершить сессию с сервером.
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void Logout();

        /// <summary>
        /// Отправляет сообщение о готовности клиента к игре
        /// </summary>
        /// <param name="request">Запрос с расстановкой кораблей</param>
        /// <returns>Сообщение о валидации готовности</returns>
        [OperationContract(IsOneWay = true)]
        void SendReady(SendReadyRequest request);

        /// <summary>
        /// Получить топ лучших игроков
        /// </summary>
        /// <returns>Топ лучших игроков</returns>
        [OperationContract(IsOneWay = true)]
        void GetTopPlayers();

        /// <summary>
        /// Отправить сообщение
        /// </summary>
        /// <param name="request">Запрос с сообщением</param>
        [OperationContract(IsOneWay = true)]
        void SendMessage(SendMessageRequest request);
        /// <summary>
        /// Получить список доступных игр
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void GetListAvailableGames();
        /// <summary>
        /// Получить список последних игр
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void GetLastGames();
    }
}

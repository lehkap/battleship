﻿using Common.Request;
using Common.Respose;
using Entity;
using System.ServiceModel;
using Entity.GameEntities;

namespace Common.Services
{
    public interface IServiceCallback
    {
        /// <summary>
        /// Вызывается после выстрела. Сообщает клиентам об успешном выстреле
        /// </summary>
        /// <param name="response">Ответ с результатом выстрела.</param>
        [OperationContract(IsOneWay = true)]
        void DoShotCallback(ShotResponse response);
        /// <summary>
        /// Отправить клиенту список доступных игр
        /// Вызывается в случае изменения листа ожидающих игр
        /// </summary>
        /// <param name="response">Ответ со списком доступных игр</param>
        [OperationContract(IsOneWay = true)]
        void GetListAvailableGamesCallback(GetListGamesResponse response);

        /// <summary>
        /// Отправить клиенту, создавшего игру информацию о подключившемся оппоненте
        /// Вызывается у клиента создавшего игру, после подключения второого игрока
        /// </summary>
        /// <param name="response">Ответ с информацией</param>
        [OperationContract(IsOneWay = true)]
        void GiveConnectedOpponentInfo(CurentGameResponse response);
        /// <summary>
        /// Отправить клиенту результат попытки присоединиться к игре.
        /// Вызывается после попытки подключиться к игре
        /// </summary>
        /// <param name="response">Ответ с информацией.</param>
        [OperationContract(IsOneWay = true)]
        void ConnectToGameCallback(CurentGameResponse response);
        /// <summary>
        /// Сообщить клиенту о результате проверки его кораблей.
        /// Вызывается, после отпрвки клиентом запроса о готовности к игре
        /// </summary>
        /// <param name="response">Результат проверки.</param>
        [OperationContract(IsOneWay = true)]
        void SendReadyCallback(SendReadyResponse response);
        /// <summary>
        /// Сообщить клиентам о начале игры(оба игрока готовы)
        /// </summary>
        /// <param name="response"></param>
        [OperationContract(IsOneWay = true)]
        void StartGame(StartGameResponse response);

        /// <summary>
        /// Сообщить клиентам об окончании игры
        /// </summary>
        /// <param name="response">Ответ с результатами игры</param>
        [OperationContract(IsOneWay = true)]
        void EndGame(EndGameResponse response);

        /// <summary>
        /// Сообщить клиенту о том, что игра прерывна
        /// Вызывается у клиента, если его опонент покинул игру или связь
        /// с ним была потеряна
        /// </summary>
        /// <param name="response">Иновормация о прерванной игре</param>
        [OperationContract(IsOneWay = true)]
        void AbortGame(AbortGameResponse response);

        /// <summary>
        /// 
        /// НЕ ИСПОЛЬЗУЕТСЯ К ФИНАЛУ УДАЛИМ
        /// 
        /// </summary>
        /// <param name="response">Информация о чате</param>
        [OperationContract(IsOneWay = true)]
        void StartChat(StartChatResponse response);

        [OperationContract(IsOneWay = true)]
        void RecieveMessage(RecieveMessageResponse response);
        /// <summary>
        /// Ответ клиенту с топом игроков.
        /// Вызывается после запроса клиента топа игроков
        /// </summary>
        /// <param name="response">Информация о топе.</param>
        [OperationContract(IsOneWay = true)]
        void GetTopPlayersCallback(GetTopPlayersResponse response);

        /// <summary>
        /// Ответ клиенту с последними играми.
        /// Вызывается после запроса о последних 100 играх
        /// </summary>
        /// <param name="response">Информация о последних играх.</param>
        [OperationContract(IsOneWay = true)]
        void GetStatisticLastGamesCallBack(GetLastGamesResponse response);

        /// <summary>
        /// Сообщает результат создания игры
        /// Вызывается после создания клиентом игры
        /// </summary>
        /// <param name="response"></param>
        [OperationContract(IsOneWay = true)]
        void CreateGameCallBack(CreateGameResponse response);

        /// <summary>  
        /// Сообщает игроку, что его оппонент готов 
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void SendOpponentIsReady(SendOpponentIsReadyResponse response);

        /// <summary>
        /// Вызывается раз в минуту, если приводит к эксепшену удаляет пользователя
        /// </summary>
        [OperationContract(IsOneWay = true)]
        void WatchDog();
    }
}

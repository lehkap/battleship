﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebClientBattleShip.Models
{
    public class ChatMessage
    {
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public string UserName { get; set; }
    }
}
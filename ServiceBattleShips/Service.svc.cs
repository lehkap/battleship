﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Timers;
using Common.DTO;
using Common.Request;
using Common.Respose;
using Common.Services;
using Entity.DataBaseEntities;
using Entity.Enums;
using Entity.GameEntities;
using Microsoft.WindowsAzure.Storage.Table.Queryable;
using ServiceBattleShips.Configuration;
using NLog;

namespace ServiceBattleShips
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public partial class Service : IService
    {
        private readonly MyContext _context = new MyContext();
        private DTOUser _user;
        private Logger _log = LogManager.GetCurrentClassLogger();
        private object _lockContext = new object();
        private Timer _watchDogTimer;

        /// <summary>
        /// Авторизация пользователя.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AuthorizeResponse Authorize(AuthorizeRequest request)
        {

            _log.Info("Authorize: {0}", request.Login);

            AuthorizeResponse response;
            try
            {
                var user =
                    _context.Users.SingleOrDefault(u => u.Login == request.Login && u.Password == request.Password);
                if (user != null) //если есть такой в бд
                {
                    var callbacks = SingletonModel.GetInstance().CallBackCollection;
                    if (callbacks.ContainsKey(user.Id)) //если на сервере уже авторизирован с таким idшником
                    {
                        FaultedClientsWorker(user.Id);
                    }
                    _user = new DTOUser {Login = user.Login, Email = user.Email, Id = user.Id};
                    response = new AuthorizeResponse {IsSuccess = true, ClientId = _user.Id};
                    SingletonModel.GetInstance()
                        .CallBackCollection.Add(_user.Id,
                            OperationContext.Current.GetCallbackChannel<IServiceCallback>());
                    _watchDogTimer = new Timer();
                    _watchDogTimer.Interval = 60000;
                    _watchDogTimer.Elapsed += WatchDogTimer_Elapsed;
                    _watchDogTimer.Enabled = true;

                }

                else // если нет в бд
                {
                    _log.Info("User: {0} not found", request.Login);
                    response = new AuthorizeResponse {IsSuccess = false, Error = "Введен неверный логин или пароль."};
                }
            }
            catch (Exception ex)
            {
                _log.Error("Authorize", ex.Message);
                response = new AuthorizeResponse
                {
                    IsSuccess = false,
                    Error = "Соединение с сервером не установленно, приходите позже."
                };
            }
            return response;
        }

        public RegisterResponse Register(RegisterRequest request)
        {
            _log.Info("Register: user {0} - email {1}", request.Login, request.Email);
            RegisterResponse response;
            try
            {
                var user = new User
                {
                    DataRegister = DateTime.Now,
                    Email = request.Email,
                    Login = request.Login,
                    Password = request.Password,
                    Id = Guid.NewGuid()
                };
                var existingUsers = _context.Users.Count(u => u.Login == user.Login || u.Email == user.Email);
                if (existingUsers != 0)
                {
                    _log.Error("Register not succsess");
                    response = new RegisterResponse()
                    {
                        IsSuccess = false,
                        Error = "Пользователь с таким логином или почтой уже существует"
                    };
                }
                else
                {
                    _context.Users.Add(user);
                    _context.SaveChanges();
                    if (EMailSender.SendEmail(user.Email, user.Login))
                        _log.Info("Message sent on email {0}", user.Email);
                    response = new RegisterResponse() {IsSuccess = true};
                }
            }
            catch (Exception ex)
            {
                _log.Error("Register", ex.Message);
                response = new RegisterResponse
                {
                    IsSuccess = false,
                    Error = "Соединение с сервером не установленно, приходите позже."
                };
            }
            return response;
        }

        public void ConnectToGame(ConnectToGameRequest request)
        {
            if (_user != null)
            {
                _log.Info(" User {0} try ConnectToGame {1}", _user.Id, request.GameId);
                if (GetCurrentGame() == null && GetAwaitingGame() == null)
                    ///проверяем играет ли уже клиент или создал ли уже он игру
                {
                    var serverStatus = SingletonModel.GetInstance();
                    CurentGameResponse response;
                    var game = serverStatus.AwaitingGames.SingleOrDefault(g => g.GameId == request.GameId);
                    if (game == null)
                    {
                        _log.Error("Game {0} not found", request.GameId);
                        response = new CurentGameResponse {Error = "Игра с таким ID не найдена.", IsSuccess = false};
                    }
                    var isAdded = game.AddOpponent(_user);
                    if (!isAdded)
                        response = new CurentGameResponse
                        {
                            Error = "К игре уже подключены два пользователя",
                            IsSuccess = false
                        };
                    else
                    {
                        serverStatus.CurrentGames.Add(game);
                        serverStatus.AwaitingGames.Remove(game);
                        var opponent =
                            serverStatus.CurrentGames.SingleOrDefault(g => g.GameId == request.GameId).Players.First();
                        serverStatus.CallBackCollection[opponent.Id].GiveConnectedOpponentInfo(new CurentGameResponse
                        {
                            IsSuccess = true,
                            Opponent = _user
                        });
                        SendGameList();
                        response = new CurentGameResponse {IsSuccess = true, Opponent = opponent};
                    }
                    GetCallback(_user).ConnectToGameCallback(response);
                }
                else
                {
                    GetCallback(_user)
                        .ConnectToGameCallback(new CurentGameResponse()
                        {
                            IsSuccess = false,
                            Error = "Вы уже сосздали игру, или уже в игре"
                        });
                }
            }
            else
            {
                GetCallback(_user).ConnectToGameCallback(SendAuthorizeError() as CurentGameResponse);
            }
        }

        public void Logout()
        {
            _log.Info(" User {0} try Logout", _user != null ? _user.Id : Guid.Empty);
            FaultedClientsWorker(_user.Id);
        }

        public void CreateGame(CreateGameRequest request)
        {
            if (_user != null)
            {
                _log.Info(" User {0} try CreateGame", _user.Id);
                var game = new DuoGame(_user);
                if (GetCurrentGame() == null && GetAwaitingGame() == null)
                    ///проверяем играет ли уже клиент или создал ли уже он игру
                {
                    SingletonModel.GetInstance().AwaitingGames.Add(game);
                    GetCallback(_user).CreateGameCallBack(new CreateGameResponse {IsSuccess = true});
                    SendGameList();
                }
                else
                {
                    _log.Error(" User {0} has already created the game. it is impossible to create a new game", _user.Id);
                    GetCallback(_user)
                        .CreateGameCallBack(new CreateGameResponse
                        {
                            IsSuccess = false,
                            Error = "Вы уже создали игру, или уже в игре"
                        });
                }

            }
            else
            {
                GetCallback(_user).CreateGameCallBack(SendAuthorizeError() as CreateGameResponse);
            }
        }

        public void LeaveGame()
        {
            if (_user != null)
            {
                _log.Info(" User {0} LeaveGame", _user.Id);
                FaultedClientsWorker(_user.Id);
                SingletonModel.GetInstance()
                    .CallBackCollection.Add(_user.Id, OperationContext.Current.GetCallbackChannel<IServiceCallback>());
            }
        }

        public void DoShot(Shot shot)
        {
            if (_user != null)
            {
                if (GetCurrentGame().NextUser == _user)
                {
                    var rShot = GetCurrentGame().Fire(GetOpponent(), shot);
                    var response = new ShotResponse
                    {
                        IsSuccess = true,
                        CurrentShot = rShot,
                        User = _user
                    };
                    if (rShot.Status == ShotStatus.Killed)
                    {
                        var killedShip =
                            GetCurrentGame().BattleMaps[GetOpponent()].ShipMap[rShot.XyCoordinate.X][
                                rShot.XyCoordinate.Y];
                        var killedDTOShip = new DTOShip()
                        {
                            Coordinates = killedShip.StartPoint,
                            DeckCount = killedShip.Decks.Length,
                            Orientation = killedShip.Orientation
                        };
                        response.KilledShipInfo = killedDTOShip;
                    }

                    foreach (var user in GetCurrentGame().Players)
                    {
                        try
                        {
                            response.NextShotUserId = GetCurrentGame().NextUser.Id;
                            GetCallback(user).DoShotCallback(response);
                        }
                        catch (CommunicationObjectAbortedException ex)
                        {
                            FaultedClientsWorker(user.Id);
                        }
                    }
                    CheckEndGame();
                }
                else
                {
                    var response = new ShotResponse
                    {
                        IsSuccess = false,
                        Error = "Дождитесь своего хода",
                        NextShotUserId = GetCurrentGame().NextUser.Id
                    };
                    try
                    {
                        GetCallback(_user).DoShotCallback(response);
                    }
                    catch (CommunicationObjectAbortedException ex)
                    {

                        FaultedClientsWorker(_user.Id);
                    }
                }
            }
            else
            {
                GetCallback(_user).DoShotCallback(SendAuthorizeError() as ShotResponse);
            }
        }

        public void GetTopPlayers()
        {
            if (_user != null)
            {
                _log.Info("User {0} try GetTopPlayers", _user.Id);
                GetTopPlayersResponse response;
                List<DTOFullUserInfo> listPlayers = new List<DTOFullUserInfo>();
                try
                {
                    if (_context != null && _context.Users != null)
                    {
                        lock (_lockContext)
                        {

                            var userList = _context.Users.OrderByDescending(p => p.Games).ToList();
                            foreach(var user in userList)
                            {
                                var fullInfoUser = new DTOFullUserInfo
                                {
                                    Id = user.Id,
                                    DataRegister = user.DataRegister,
                                    Games = user.Games,
                                    Login = user.Login,
                                    PercentWonGames = user.Games != 0
                                                     ? (Convert.ToInt32(((double)user.WonGames / (double)user.Games) * 100))
                                                     : 0
                                };
                                listPlayers.Add(fullInfoUser);
                            }                          
                        }
                    }
                    response = new GetTopPlayersResponse
                    {
                        IsSuccess = true,
                        TopPlayers = listPlayers
                    };
                }
                catch (Exception ex)
                {
                    _log.Error("GetTopPlayers error {0}", ex.Message);
                    response = new GetTopPlayersResponse
                    {
                        IsSuccess = false,
                        Error = "Произошла ошибка при чтении списка рейтинга игроков"
                    };
                }
                GetCallback(_user).GetTopPlayersCallback(response);
            }
            else
            {
                GetCallback(_user).GetTopPlayersCallback(SendAuthorizeError() as GetTopPlayersResponse);
            }
        }

        public void SendMessage(SendMessageRequest request)
        {
            try
            {
                if (_user != null)
                {
                    _log.Info("User {0} SendMessage", _user.Id);
                    var responseMessage = new RecieveMessageResponse();
                    responseMessage.Message = request.Message;
                    if (GetOpponent() != null)
                    {
                        responseMessage.DateTime = DateTime.Now;
                        responseMessage.Name = _user.Login;
                        GetCallback(GetOpponent()).RecieveMessage(responseMessage);
                    }
                }
                else
                {
                    GetCallback(_user).RecieveMessage(SendAuthorizeError() as RecieveMessageResponse);
                }
            }
            catch (CommunicationObjectAbortedException)
            {

                FaultedClientsWorker(GetOpponent().Id);
            }

        }

        /// <summary>
        /// Получить лист последних игр.
        /// </summary>
        public void GetListAvailableGames()
        {
            List<DTOAwaitingGame> dtoAwaitingGame = new List<DTOAwaitingGame>();
            if (_user != null)
            {
                _log.Info("GetListAvailableGames for user {0}", _user.Id);
                var games = GetAwaitingGamesList();
                if (games != null)
                {
                    dtoAwaitingGame = games.Select(t => new DTOAwaitingGame()
                    {
                        Creator = t.Players.First(),
                        CreationTime = t.CreateDateTime,
                        GameId = t.GameId
                    }).ToList();
                }
                var response = new GetListGamesResponse()
                {
                    IsSuccess = true,
                    Games = dtoAwaitingGame
                };
                GetCallback(_user).GetListAvailableGamesCallback(response);
            }
            else
            {
                GetCallback(_user).GetListAvailableGamesCallback(SendAuthorizeError() as GetListGamesResponse);
            }
        }

        public void SendReady(SendReadyRequest request)
        {
            if (_user != null)
            {
                _log.Info("User {0} SendReady", _user.Id);
                SendReadyResponse response;
                var battleMap = new BattleMap();
                var ships = ShipBuilder(request.Ships);
                battleMap.GetClientShipCollection(ships);
                if (battleMap.ConflictShip.Count > 0)
                {
                    _log.Error("Improper alignment of ships for User {0}", _user.Id);
                    response = new SendReadyResponse
                    {
                        IsSuccess = false,
                        ConflictShip = battleMap.ConflictShip,
                        Error = "Неправильная расстановка кораблей"
                    };
                    try
                    {
                        GetCallback(_user).SendReadyCallback(response);
                    }
                    catch (CommunicationObjectAbortedException)
                    {
                        FaultedClientsWorker(_user.Id);
                    }

                }
                else
                {
                    response = new SendReadyResponse {IsSuccess = true};
                    try
                    {
                        GetCurrentGame().AddBattleMap(_user, battleMap);
                        GetCallback(_user).SendReadyCallback(response);
                    }
                    catch (CommunicationObjectAbortedException ex)
                    {
                        FaultedClientsWorker(_user.Id);
                    }
                    try
                    {
                        GetCallback(GetOpponent())
                            .SendOpponentIsReady(new SendOpponentIsReadyResponse() {IsSuccess = true});
                    }
                    catch (CommunicationObjectAbortedException)
                    {
                        FaultedClientsWorker(GetOpponent().Id);
                    }
                    CheckClientsReady();
                }
            }
            else
            {
                GetCallback(_user).SendReadyCallback(SendAuthorizeError() as SendReadyResponse);
            }
        }

        public void GetLastGames()
        {
            GetLastGamesResponse response;
            List<DTOFullGameInfo> listGames = new List<DTOFullGameInfo>();
            if (_user != null)
            {
                _log.Info("User {0} try GetLastGames", _user.Id);
                try
                {
                    if (_context != null && _context.Games != null)
                    {
                        List<Game> games = null;
                        lock (_lockContext)
                        {
                            games = _context.Games
                                .OrderByDescending(g => g.EndDateTime)
                                .Take(100).ToList();

                            if (games != null)
                            {
                                foreach (var g in games)
                                {
                                    var player1 = _context.Users.Where(u => u.Id == g.IdPlayer1).FirstOrDefault();
                                    var player2 = _context.Users.Where(u => u.Id == g.IdPlayer2).FirstOrDefault();
                                    var winner = _context.Users.Where(u => u.Id == g.IdWinner).FirstOrDefault();
                                    var newDTOGame = new DTOFullGameInfo
                                    {
                                        Id = g.Id,
                                        NamePlayer1 = player1 != null ? player1.Login : "не найден",
                                        NamePlayer2 = player2 != null ? player2.Login : "не найден",
                                        Winner = winner != null ? winner.Login : "не найден",
                                        StartGame = g.StartDateTime,
                                        LongTime = (g.EndDateTime - g.StartDateTime).Minutes.ToString()
                                    };
                                    listGames.Add(newDTOGame);
                                }
                            }
                        }

                    }
                    response = new GetLastGamesResponse
                    {
                        IsSuccess = true,
                        Games = listGames
                    };
                }
                catch (Exception ex)
                {
                    _log.Error("Error GetLastGames for User {0} Exception: {1}", _user.Id, ex.Message);
                    response = new GetLastGamesResponse
                    {
                        IsSuccess = false,
                        Error = "Произошла ошибка при чтении списка последних 100 игр"
                    };
                }
                GetCallback(_user).GetStatisticLastGamesCallBack(response);
            }
            else
            {
                GetCallback(_user).GetStatisticLastGamesCallBack(SendAuthorizeError() as GetLastGamesResponse);
            }
        }

        private void WatchDogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                GetCallback(_user).WatchDog();
            }
            catch (Exception)
            {
                _watchDogTimer.Elapsed -= WatchDogTimer_Elapsed;
                FaultedClientsWorker(_user.Id);
                OperationContext.Current.Channel.Close();
            }
        }


    }
}
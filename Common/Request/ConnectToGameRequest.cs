﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Request
{
    public class ConnectToGameRequest:BaseRequest
    {
        [DataMember]
        public Guid GameId { get; set; }
    }
}

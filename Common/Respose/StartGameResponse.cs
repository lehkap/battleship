﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Respose
{
    public class StartGameResponse:BaseResponse
    {
        /// <summary>
        /// Id пользователя, чей сейчас ход
        /// </summary>
        [DataMember]
        public Guid NextShotUserId { get; set; }
    }
}

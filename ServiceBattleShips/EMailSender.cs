﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace ServiceBattleShips
{
    public static class EMailSender
    {
        private static readonly string _emailSender = "EpamBattleShips@gmail.com";
        private static readonly string _password = "7!14_h55";
        private static readonly string _serviceAddress = "smtp.gmail.com";
        private static readonly string _subjectRegistry = "Регистрация на сайте 'Морской бой'";
        private static readonly string _messageWelcome = "Здравствуйте, {0}! Мы рады приветствовать вас на нашем сайте 'Морской бой'! Сражайтесь и побеждайте!";

        /// <summary>
        /// Отправление сообщения по Email
        /// </summary>
        /// <param name="eMailRecipient">Emailполучателя</param>
        /// <param name="login">Логин</param>
        /// <param name="subject">По умолчанию - регистрация</param>
        /// <param name="message"> по умолчанию приветственное сообщение</param>
        /// <returns></returns>
        public static bool SendEmail(string eMailRecipient, string login, string subject = default(string), string message = default(string))
        {
            if (message == default(string))
            {
                message = String.Format(_messageWelcome, login);
            }

            if (subject == default(string))
            {
                subject = _subjectRegistry;
            }

            try
            {
                MailAddress from = new MailAddress(_emailSender, "Морской бой");
                MailAddress to = new MailAddress(eMailRecipient);
                MailMessage m = new MailMessage(from, to);
                // тема письма
                m.Subject = subject;
                // текст письма
                m.Body = message;
                // адрес smtp-сервера и порт, с которого будем отправлять письмо
                SmtpClient smtp = new SmtpClient(_serviceAddress);
                // логин и пароль отправителя
                smtp.Credentials = new NetworkCredential(_emailSender, _password);
                smtp.EnableSsl = true;
                smtp.Send(m);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}
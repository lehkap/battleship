﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebClientBattleShip.GameService;

namespace WebClientBattleShip.Models
{
    public class SingletonModel
    {
        private static SingletonModel instance;
        private static object locker = new object();

        private SingletonModel()
        {
            Clients = new Dictionary<Guid, Client>();
        }
        public Dictionary<Guid,Client> Clients { get; }

        public static SingletonModel GetInstance()
        {

            if (instance == null)
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new SingletonModel();
                    }
                }
            }
            return instance;
        }

        public void ClearByGuid(Guid guid)
        {
            if (Clients.ContainsKey(guid))
            {
                Clients.Remove(guid);
            }
        }
    }
}
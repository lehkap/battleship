﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Respose
{
    public class RecieveMessageResponse :BaseResponse
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public DateTime DateTime { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}

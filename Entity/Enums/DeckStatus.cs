﻿using System.Runtime.Serialization;

namespace Entity.Enums
{
    /// <summary>
    /// Статус палубы.
    /// </summary>
    [DataContract]
    public enum DeckStatus
    {
        [EnumMember]
        Ok,
        [EnumMember]
        Damaged
    }
}

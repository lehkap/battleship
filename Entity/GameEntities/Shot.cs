﻿using System.Runtime.Serialization;
using Entity.Enums;

namespace Entity.GameEntities
{
    /// <summary>
    /// Выстрел.
    /// </summary>
    [DataContract]
    public class Shot
    {
        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public Shot(XYCoordinate xyCoordinate)
        {
            XyCoordinate = xyCoordinate;
        }

        [DataMember]
        public XYCoordinate XyCoordinate { get; private set; }

        [DataMember]
        public ShotStatus Status { get; set; }

    }
}

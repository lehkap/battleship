﻿using Common.DTO;
using Entity.GameEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Respose
{
    public class ShotResponse : BaseResponse
    {
        [DataMember]
        public Shot CurrentShot { get; set; }

        /// <summary>
        /// тот кто сделал выстрел
        /// </summary>
        [DataMember]
        public DTOUser User{get;set; }

        /// <summary>
        /// Id пользователя, чей сейчас ход
        /// </summary>
        [DataMember]
        public Guid NextShotUserId { get; set; }

        /// <summary>
        /// Если корабль убитздесь будет инфа о нем
        /// просили тиммейты
        /// </summary>
        [DataMember]
        public DTOShip KilledShipInfo { get; set; }
    }
}

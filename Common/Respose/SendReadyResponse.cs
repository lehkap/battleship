﻿using Entity.GameEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.Respose
{
    public class SendReadyResponse:BaseResponse
    {
        [DataMember]
        public ICollection<Ship> ConflictShip { get; set; }
    }
}

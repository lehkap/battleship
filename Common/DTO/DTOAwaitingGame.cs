﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO
{
    [DataContract]
    public class DTOAwaitingGame
    {
        [DataMember]
        public Guid GameId { get; set; }
        [DataMember]
        public DTOUser Creator { get; set; }
        [DataMember]
        public DateTime CreationTime { get; set; }
    }
}

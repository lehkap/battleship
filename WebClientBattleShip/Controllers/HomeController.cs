﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebClientBattleShip.GameService;
using WebClientBattleShip.Models;

namespace WebClientBattleShip.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Главная страница сайта
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.IsLogined = client.IsAuthorised;
            ViewBag.NameUser = client.Name;
            return View();
        }
        /// <summary>
        /// Страница авторизации пользователя
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="password">пароль</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login(string login, string password)
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            if (client.IsAuthorised) // Если авторизован - уходи!
            {
                return new RedirectResult("~/Home/Index");
            }
            if (login != null && password != null) // Если логин и пароль не пустые
            {
                var request = new AuthorizeRequest() { Login = login, Password = password };
                var response = client.ServiceClient.Authorize(request);
                if (response.IsSuccess) // Если вернули ошибку авторизации
                {
                    var clientsAndKey = SingletonModel.GetInstance()
                        .Clients.FirstOrDefault(c => c.Value.IsAuthorised && c.Value.ClientId == response.ClientId);
                    if (!clientsAndKey.Equals(default(KeyValuePair<Guid, Client>)))
                    {
                        SingletonModel.GetInstance()
                            .Clients.Remove(clientsAndKey.Key);
                    }
                    client.Authorize(response.ClientId);
                    client.Name = login;
                    ViewBag.IsLogined = true;
                    return new RedirectResult("~/Game");
                }
                ViewBag.Error = response.Error;
            }
            ViewBag.IsLogined = false;
            return View();
        }

        /// <summary>
        /// Выходи из профиля
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Logout()
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            if (client.IsAuthorised)
            {
                client.ServiceClient.Logout();
            }
            var guid = this.GetGuidFromCookie();
            SingletonModel.GetInstance().ClearByGuid(guid);
            var cookie = new HttpCookie("UserInfo") {Expires = DateTime.Now.AddDays(-1d)}; // Делаем куки авторизации вчерашним днем(удаляем)
            Response.Cookies.Add(cookie);
            ViewBag.IsLogined = false;
            return new RedirectResult("~/Home/Index");
        }
        /// <summary>
        /// Регистрация пользователя
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        /// <param name="email">Мыло</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Register(string login, string password, string email)
        {
            var userid = this.WorkWithCookie();
            var client = SingletonModel.GetInstance().Clients[userid];
            ViewBag.NameUser = client.Name;
            if (client.IsAuthorised)
            {
                return new RedirectResult("~/Home/Index");
            }
            if (login != null && password != null && email != null)
            {
                var response =
                    client.ServiceClient.Register(new RegisterRequest()
                    {
                        Login = login,
                        Password = password,
                        Email = email
                    });
                if (response.IsSuccess)
                {
                    return new RedirectResult($"~/Home/Login?login={login}&password={password}"); // После регистрации проходит автоматический логин
                }
                ViewBag.Error = response.Error;
            }
            ViewBag.IsSuccessRegister = false;
            ViewBag.IsLogined = false;
            return View();
        }    
    }
}
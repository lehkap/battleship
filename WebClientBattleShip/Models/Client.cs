﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using WebClientBattleShip.GameService;

namespace WebClientBattleShip.Models
{
    public class Client
    {
        public Client(ClientCallback callback)
        {
            Callbacks = callback;
            ServiceClient = new ServiceClient(new InstanceContext(callback), "NetTcpBinding_IService");
            ChatMessage = new List<ChatMessage>();
        }

        private ServiceClient _serviceClient;
        public Guid ClientId { get; private set; }
        public bool IsAuthorised { get; private set; }
        public ClientCallback Callbacks { get; }

        public ServiceClient ServiceClient
        {
            get
            {
                if (_serviceClient.State == CommunicationState.Faulted)
                {
                    _serviceClient = new ServiceClient(new InstanceContext(Callbacks), "NetTcpBinding_IService");
                }
                return _serviceClient;
            }
            private set { _serviceClient = value; }

        }
        public string Name { get; set; }
        public ICollection<ChatMessage> ChatMessage { get; set; }
        public void Authorize(Guid clientId)
        {
            ClientId = clientId;
            IsAuthorised = true;
        }
    }
}
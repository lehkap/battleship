﻿function connectToGame()
{
    $.ajax({
        url: '/Game/Play?gameId=' + $(':radio').filter(':checked').attr('id'),
        success: function () {
            $("#connectError").css("display", "none");
            var timerCheckOpponent = setInterval(function() {
                $.get('/Game/CheckOpponent/', function(data) {
                    if (data !== "") {
                        $("#content").html(data);
                        clearInterval(timerCheckOpponent);
                        clearInterval(timerId);
                    }
                });
            }, 5000);
        },
        statusCode: {
            400: function() {
                $("#connectError").css("display", "block");
            }
        }
    });

};


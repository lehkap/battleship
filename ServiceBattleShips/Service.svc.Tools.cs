﻿using Common.DTO;
using Common.Respose;
using Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using Entity.DataBaseEntities;
using Entity.GameEntities;
using NLog;

namespace ServiceBattleShips
{
    public partial class Service 
    {
        /// <summary>
        /// Возвращает список доступных для подключения игр
        /// </summary>
        /// <returns>Список доступных игр</returns>
        private List<DuoGame> GetAwaitingGamesList()
        {
            var serverStatus = SingletonModel.GetInstance();
            return serverStatus.AwaitingGames;
        }
        /// <summary>
        /// Возвращает текущую игру для экземпляра сервиса вызвавшего метод
        /// </summary>
        /// <returns>Текущая игра</returns>
        private DuoGame GetCurrentGame()
        {
            var serverStatus = SingletonModel.GetInstance();
            return serverStatus.CurrentGames.FirstOrDefault(g => g.Players.Any(p => p.Id == _user.Id));
        }
        /// <summary>
        /// Возвращает игру, ожидающую подключения второго игрока
        /// </summary>
        /// <returns>Игра, ожидающая подключения</returns>
        private DuoGame GetAwaitingGame()
        {
            var serverStatus = SingletonModel.GetInstance();
            return serverStatus.AwaitingGames.FirstOrDefault(g => g.Players.Any(p => p == _user));
        }
        /// <summary>
        /// Возвращает данные оппонента
        /// </summary>
        /// <returns>Данные оппонента</returns>
        private DTOUser GetOpponent()
        {
            return GetCurrentGame()?.Players.First(p => p != _user);
        }
        /// <summary>
        /// Возвращает коллбеки для указанного клиента
        /// </summary>
        /// <param name="user">Клиент, для которого запрашиваются колбеки</param>
        /// <returns>Колбеки</returns>
        private IServiceCallback GetCallback(DTOUser user)
        {
            var serverStatus = SingletonModel.GetInstance();
            return serverStatus.CallBackCollection[user.Id];
        }
        /// <summary>
        /// Проверяет готовность игроков к игре, и при готовности к игре
        /// вызывает у клиентов коллбек на старт игры
        /// </summary>
        private void CheckClientsReady()
        {
            if (GetCurrentGame().AllClientsIsReady())
            {
                foreach (var user in GetCurrentGame().Players)
                {
                    GetCallback(user).StartGame(new StartGameResponse() { IsSuccess=true, NextShotUserId=GetCurrentGame().NextUser.Id});
                }
            }
        }
        /// <summary>
        /// Проверяет игровые поля на отсутствие живых кораблей
        /// При отсутствии у одного из игроков живых короблей, сообщает клиентам
        /// о конце игры
        /// </summary>
        private void CheckEndGame()
        {
            if (GetCurrentGame().IsGameOver)
            {
                foreach (var user in GetCurrentGame().Players)
                {
                    _log.Info("GameOver {0}", user.CurrentGameId);
                    var _response = new EndGameResponse
                    {
                        IsSuccess = true,
                        Winner = GetCurrentGame().Winner
                    };
                    GetCallback(user).EndGame(_response);
                }
                StatisticWorker(GetCurrentGame());
                SingletonModel.GetInstance().CurrentGames.Remove(GetCurrentGame());
            }
        }
        /// <summary>
        /// Метод управляет рассылкой колбека сос списком доступных игр
        /// </summary>
        private void SendGameList()
        {
            var listBuilder = new ListBuilder();
            var response = new GetListGamesResponse
            {
                IsSuccess = true,
                Games = listBuilder.GetGameList(SingletonModel.GetInstance().AwaitingGames)
            };
            KeyValuePair<Guid, IServiceCallback> item = new KeyValuePair<Guid, IServiceCallback>();
            //Здесь специально FOR. Колекция чиститься. 
            //Во время foreach удалять добалять нельзя, иначе Exception
            for (int i = 0; i < SingletonModel.GetInstance().CallBackCollection.Count; i++)
            {
                try
                {
                    item = SingletonModel.GetInstance().CallBackCollection.ElementAt(i);
                    item.Value.GetListAvailableGamesCallback(response);
                }
                catch (CommunicationObjectAbortedException ex)
                {
                    FaultedClientsWorker(item.Key);
                }
            }
        }
        /// <summary>
        /// Отправка клиенту ошибки авторизации.
        /// </summary>
        private BaseResponse SendAuthorizeError()
        {
            var response = new AuthorizeResponse() { Error = "Пользователь не прошел авторизацию. Сначало авторизируйтесь методом Authorize", IsSuccess = false};
            //OperationContext.Current.GetCallbackChannel<IServiceCallback>().AuthorizeCallback(response);
            return response;
        }
        /// <summary>
        /// Вычищает сервер от пользователя покинувшего игру
        /// </summary>
        /// <param name="userId">Пара пользователь-колбеки</param>
        private void FaultedClientsWorker(Guid userId)
        {
            var awaitingGames = SingletonModel.GetInstance().AwaitingGames;
            var currentGames = SingletonModel.GetInstance().CurrentGames;
            var currentGame = currentGames.FirstOrDefault(g => g.Players.Any(p => p.Id == userId));
            var awaitingGame = awaitingGames.FirstOrDefault(g => g.Players.Any(p => p.Id == userId));
            SingletonModel.GetInstance().CallBackCollection.Remove(userId);
            if (awaitingGame != null)
            {
                awaitingGames.Remove(awaitingGame);
                SendGameList();
            }
            if (currentGame != null)
            {
                currentGame.Winner = currentGame.Players.FirstOrDefault(p => p.Id != userId);
                currentGame.IsGameOver = true;
                GetCallback(currentGame.Winner).AbortGame(new AbortGameResponse() { IsSuccess = true, Error = "Противник покинул игру, вы победили" });
                StatisticWorker(GetCurrentGame());
                currentGames.Remove(currentGame);
                SendGameList();
            }
        }

        /// <summary>
        /// Строит игровые корабли из DTOShip
        /// </summary>
        /// <param name="ships"></param>
        /// <returns></returns>
        private List<Ship> ShipBuilder(DTOShip[] ships)
        {
            List<Ship> rShips=new List<Ship>();
            foreach (var ship in ships)
            {
                rShips.Add(new Ship(ship.DeckCount,ship.Coordinates,ship.Orientation));
            }
            return rShips;
        }
        /// <summary>
        /// Обновляет статистику на основе результатов игры
        /// </summary>
        /// <param name="game">Игра</param>
        private void StatisticWorker(DuoGame game)
        {
            _log.Info("Add info about ended game");
            try
            {
                var endedGame = new Game()
                {
                    CreateDateTime = game.CreateDateTime,
                    StartDateTime = game.StartGameDateTime,
                    EndDateTime = DateTime.Now,
                    Id = game.GameId,
                    IdPlayer1 = game.Players[0].Id,
                    IdPlayer2 = game.Players[1].Id,
                    IdWinner = game.Winner.Id
                };
                _context.Games.Add(endedGame);

                var id = game.Players[0].Id;
                var user1 = _context.Users.Where(u => u.Id == id).FirstOrDefault();
                if (user1 != null)
                {
                    _log.Info("Adedd game count to user id {0}", user1.Id);
                    user1.Games++;
                    var entry = _context.Entry(user1);
                    entry.Property(e => e.Games).IsModified = true;
                }
                id = game.Players[1].Id;
                var user2 = _context.Users.Where(u => u.Id == id).FirstOrDefault();
                if (user2 != null)
                {
                    _log.Info("Adedd game count to user id {0}", user2.Id);
                    user2.Games++;
                    var entry2 = _context.Entry(user2);
                    entry2.Property(e => e.Games).IsModified = true;
                }
                id = game.Winner.Id;
                var winner = _context.Users.Where(u => u.Id == id).FirstOrDefault();
                if (winner != null)
                {
                    _log.Info("Adedd game count to winner id {0}", winner.Id);
                    winner.WonGames++;
                    var entry3 = _context.Entry(winner);
                    entry3.Property(e => e.WonGames).IsModified = true;
                }

                _context.SaveChanges();
            }
            catch(Exception ex)
            {
                _log.Error("StatisticWorker Error: {0}", ex.Message);
            }
        }
    }
}
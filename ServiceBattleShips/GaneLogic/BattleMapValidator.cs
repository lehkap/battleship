﻿using Entity.GamesEntities2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceBattleShips.GaneLogic
{
    public class BattleMapValidator
    {
        /// <summary>
        /// Лист с точками с неправильным расположением кораблей
        /// </summary>
        public List<XYCoordinate> ConflictCoordinates { get; set; }

        /// <summary>
        /// Проверяет расположение кораблей на валидность и заносит невалидные координаты в ConflictCoordinates
        /// </summary>
        /// <returns>true-валиден false-нет</returns>
        public bool isValid(BattleMap battleMap)
        {
            foreach (var ship in battleMap.ShipsRegistry)
            {
                foreach (var deck in ship.Decks)
                {
                    if (!pointValidator(battleMap, deck.Coordinate))
                    {
                        ConflictCoordinates.Add(deck.Coordinate);
                    }
                }
            }
            if (ConflictCoordinates.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        /// <summary>
        /// Проверяет координату на валидность
        /// </summary>
        /// <param name="coordinate">Координата</param>
        /// <returns>true-валиден false-нет</returns>
        bool pointValidator(BattleMap battleMap, XYCoordinate coordinate)
        {

            var checkList = GetPointCheckPool(coordinate);
            foreach (var point in checkList)
            {
                if (battleMap.Map[point.X, point.Y] != null && battleMap.Map[point.X, point.Y] != Map[coordinate.X, coordinate.Y])
                {
                    return false;
                }
            }
            return true;

        }

        /// <summary>
        /// Гинерит список точек(точки вокрук исследуемой точки) для проверки
        /// </summary>
        /// <param name="coordinate">Координата</param>
        /// <returns>Список координат вокруг</returns>
        List<XYCoordinate> GetPointCheckPool(XYCoordinate coordinate)
        {
            List<XYCoordinate> _result = new List<XYCoordinate>();
            List<XYCoordinate> _preResult = new List<XYCoordinate>();
            _preResult.Add(new XYCoordinate { X = coordinate.X - 1, Y = coordinate.Y + 0 });
            _preResult.Add(new XYCoordinate { X = coordinate.X - 1, Y = coordinate.Y + 1 });
            _preResult.Add(new XYCoordinate { X = coordinate.X + 0, Y = coordinate.Y + 1 });
            _preResult.Add(new XYCoordinate { X = coordinate.X + 1, Y = coordinate.Y + 1 });
            _preResult.Add(new XYCoordinate { X = coordinate.X + 1, Y = coordinate.Y + 0 });
            _preResult.Add(new XYCoordinate { X = coordinate.X + 1, Y = coordinate.Y - 1 });
            _preResult.Add(new XYCoordinate { X = coordinate.X + 0, Y = coordinate.Y - 1 });
            _preResult.Add(new XYCoordinate { X = coordinate.X - 1, Y = coordinate.Y - 1 });
            foreach (var point in _preResult)
            {
                if (point.X >= 0 && point.X < 10 && point.Y >= 0 && point.Y < 10)
                {
                    _result.Add(point);
                }
            }
            return _result;
        }
    }
}
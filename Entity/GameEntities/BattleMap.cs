﻿using Entity.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.PeerResolvers;
using System.Text;
using System.Threading.Tasks;

namespace Entity.GameEntities
{
    [DataContract]
    public class BattleMap
    {
        /// <summary>
        /// Корабли
        /// </summary>
        [DataMember]
        public ICollection<Ship> Ships { get; }
        /// <summary>
        /// Корабли на карте
        /// </summary>
        [DataMember]
        public Ship[][] ShipMap { get; }
        /// <summary>
        /// Зона вокруг кораблей
        /// </summary>
        [DataMember]        
        public Ship[][] ShipZone { get; }

        /// <summary>
        /// Список конфликтных кораблей
        /// </summary>
        public List<Ship> ConflictShip { get; set; }

        public bool IsShipsAlive
        {
            get { return Ships.Any(s => !s.IsKilled); }
        }
        public BattleMap()
        {            
            ConflictShip = new List<Ship>();
            Ships = new List<Ship>();
            ShipMap = new Ship[10][];
            ShipZone = new Ship[10][];
            for (int i = 0; i < 10; i++)
            {
                ShipMap[i] = new Ship[10];
                ShipZone[i] = new Ship[10];
            }            
        }
        /// <summary>
        /// Регистрация корабля
        /// </summary>
        /// <param name="ship">Корабль</param>
        /// <returns>Если удалось зарегистировать без ошибок корабль - возвращает true</returns>
        public bool Register(Ship ship)
        {
            var checkAvailable = CheckAvailable(ship);
            if (checkAvailable)
            {
                var xStart = ship.StartPoint.X;
                var yStart = ship.StartPoint.Y;
                Ships.Add(ship);
                for (var i = 0; i < ship.Decks.Length; i++)
                {
                    if (ship.Orientation == ShipOrientation.Horisontal)
                    {
                        ShipMap[xStart + i][yStart] = ship; 
                        //ShipZone[xStart - 1 + i][yStart - 1] = ship; //Выше 
                        //ShipZone[xStart - 1 + i][yStart + 1] = ship; //Ниже
                        //ShipZone[xStart - 1][yStart] = ship; //Левее
                        //ShipZone[xStart + ship.Decks.Length+1][yStart] = ship; //Правее
                    }
                    else
                    {
                        ShipMap[xStart][yStart + i] = ship;
                        //ShipZone[xStart - 1][yStart - 1 + i] = ship; //Левее 
                        //ShipZone[xStart + 1][yStart + 1 + i] = ship; //Правее
                        //ShipZone[xStart][yStart - 1] = ship; //Ниже
                        //ShipZone[xStart][yStart + ship.Decks.Length + 1] = ship; //Выше
                    }
                }
            }
            else
            {
                ConflictShip.Add(ship);
            }
            return checkAvailable;
        }
        /// <summary>
        /// Проверка кораблей на правильность размещения и количество одинаковых
        /// </summary>
        /// <param name="ship">Корабль</param>
        /// <returns>Возвращает true если размещение может пройти удачно.</returns>
        public bool CheckAvailable(Ship ship)
        {
            // Проверяем количество кораблей размещенных с таким количеством палуб.
            if (Ships.Count(s => s.Decks.Length == ship.Decks.Length) >= 5 - ship.Decks.Length)
                return false;
            //проверяем начальная точка находится в границах поля
            if (ship.StartPoint.X >= 0 && ship.StartPoint.X < 10 && ship.StartPoint.Y >= 0 && ship.StartPoint.Y < 10)
            {
                for (var i = 0; i < ship.Decks.Length; i++)
                {
                    // Проверяем нет ли кораблей по этим координатам а так же их зоны.
                    if (ship.Orientation == ShipOrientation.Horisontal)
                    {
                        if (ShipMap[ship.StartPoint.X + i][ship.StartPoint.Y] != null ||
                            ShipZone[ship.StartPoint.X + i][ship.StartPoint.Y] != null && ship.StartPoint.X+i>=10)
                            return false;
                    }
                    else // if ship.Orientation == ShipOrientation.Vertical
                    {
                        if (ShipMap[ship.StartPoint.X][ship.StartPoint.Y + i] != null ||
                            ShipZone[ship.StartPoint.X][ship.StartPoint.Y + i] != null && ship.StartPoint.Y+i>=10)
                            return false;
                    }
                }
            }
            return true;
        }
        /// <summary>
        /// Сделать выстрел по карте.
        /// </summary>
        /// <param name="shot">Выстрел.</param>
        /// <returns>Статус выстрела.</returns>
        public ShotStatus Fire(Shot shot)
        {
            var ship = ShipMap[shot.XyCoordinate.X][shot.XyCoordinate.Y];
            if (ship != null)
            {
                if (ship.Orientation == ShipOrientation.Horisontal)
                {
                    var numDeck = shot.XyCoordinate.X - ship.StartPoint.X;
                    ship.Fire(numDeck);
                }
                if (ship.Orientation == ShipOrientation.Vertical)
                {
                    var numDeck = shot.XyCoordinate.Y - ship.StartPoint.Y;
                    ship.Fire(numDeck);
                }
                return ship.IsKilled ? ShotStatus.Killed : ShotStatus.Hit;
            }
            return ShotStatus.Missed;
        }

        /// <summary>
        /// Располагает корабли на поле боя
        /// </summary>
        /// <param name="ships">Коллекция кораблей</param>
        public void GetClientShipCollection(ICollection<Ship> ships)
        {
            foreach(var ship in ships)
            {
                Register(ship);
            }
        }
    }
}

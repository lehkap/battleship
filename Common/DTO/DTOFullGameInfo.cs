﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO
{
    [DataContract]
    public class DTOFullGameInfo
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public DateTime StartGame { get; set; }

        [DataMember]
        public string LongTime { get; set; }

        [DataMember]
        public string NamePlayer1 { get; set; }

        [DataMember]
        public string NamePlayer2 { get; set; }

        [DataMember]
        public string Winner { get; set; }
    }
}

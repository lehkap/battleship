﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebClientBattleShip.Models.DtoModels
{
    [Serializable]
    public class ShotResult
    {
        public bool IsYourNext { get; set; }
        public bool IsYourStep { get; set; }

        public bool IsKilled { get; set; }
        public bool IsMissed { get; set; }

        public int X { get; set; }
        public int Y { get; set; }
        public Ships Ship { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebClientBattleShip.GameService;

namespace WebClientBattleShip.Models
{
    public class ClientCallback: IServiceCallback
    {
        public ClientCallback()
        {
            NewResponses = new List<BaseResponse>();
        }

        public ICollection<BaseResponse> NewResponses { get; }
        public ICollection<BaseResponse> GetResponses(Type baseResponseType)
        {
            return NewResponses.Where(r => r.GetType() == baseResponseType).ToList();
        }
        public void ClearResponses(ICollection<BaseResponse> response)
        {
            foreach (var r in response)
            {
                NewResponses.Remove(r);
            }
        }
        public void AbortGame(AbortGameResponse response)
        {
            NewResponses.Add(response);
        }

        public void ConnectToGameCallback(CurentGameResponse response)
        {
            NewResponses.Add(response);
        }

        public void CreateGameCallBack(CreateGameResponse response)
        {
            NewResponses.Add(response);
        }

        public void DoShotCallback(ShotResponse response)
        {
            NewResponses.Add(response);
        }

        public void EndGame(EndGameResponse response)
        {
            NewResponses.Add(response);
        }

        public void GetListAvailableGamesCallback(GetListGamesResponse response)
        {
            NewResponses.Add(response);
        }

        public void GetStatisticLastGamesCallBack(GetLastGamesResponse response)
        {
            NewResponses.Add(response);
        }

        public void GetTopPlayersCallback(GetTopPlayersResponse response)
        {
            NewResponses.Add(response);
        }

        public void GiveConnectedOpponentInfo(CurentGameResponse response)
        {
            NewResponses.Add(response);
        }

        public void RecieveMessage(RecieveMessageResponse response)
        {
            NewResponses.Add(response);
        }

        public void SendOpponentIsReady(SendOpponentIsReadyResponse response)
        {
            NewResponses.Add(response);
        }

        public void WatchDog()
        {
            
        }

        public void SendReadyCallback(SendReadyResponse response)
        {
            NewResponses.Add(response);
        }

        public void StartChat(StartChatResponse response)
        {
            NewResponses.Add(response);
        }

        public void StartGame(StartGameResponse response)
        {
            NewResponses.Add(response);
        }
    }
}
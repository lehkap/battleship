﻿using Common.DTO;
using Common.Services;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entity.DataBaseEntities;


namespace ServiceBattleShips
{
    public class SingletonModel
    {
        private static SingletonModel instance;
        private static object locker = new object();

        private SingletonModel()
        {
            CallBackCollection = new Dictionary<Guid, IServiceCallback>();
            CurrentGames = new List<DuoGame>();
            AwaitingGames = new List<DuoGame>();            
        }

        public Dictionary<Guid, IServiceCallback> CallBackCollection { get; set; } 

        public List<DuoGame> CurrentGames { get; set; }

        public List<DuoGame> AwaitingGames { get; set; }

        public static SingletonModel GetInstance()
        {
            if (instance == null)
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new SingletonModel();
                    }
                }
            }
            return instance;
        }
    }
}
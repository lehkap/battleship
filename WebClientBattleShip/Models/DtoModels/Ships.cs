﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebClientBattleShip.Models.DtoModels
{
    [Serializable]
    public class Ships
    {
        public string id { get; set; }

        public int length { get; set; }

        public string position { get; set; }
        public int x { get; set; }
        public int y { get; set; }
    }
}